package com.margin.auth.model;

import com.margin.entity.ApiUserDetail;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationResponse{
    private ApiUserDetail apiUserDetail;
    private String token;
}
