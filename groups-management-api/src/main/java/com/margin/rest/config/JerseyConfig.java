package com.margin.rest.config;

import com.margin.rest.group.impl.GroupEndpointImpl;
import com.margin.rest.info.impl.InfoEndpointImpl;
import com.margin.rest.user.impl.UserEndpointImpl;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;


@Component
@ApplicationPath("/api/v1")
public class JerseyConfig extends AbstractJerseyConfig {

    public JerseyConfig() {
        super();
        registerEndpoints();
    }

    private void registerEndpoints() {
        register(InfoEndpointImpl.class);
        register(UserEndpointImpl.class);
        register(GroupEndpointImpl.class);
    }
}
