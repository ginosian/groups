package com.margin.rest.group;


import com.margin.dto.ListResponseDTO;
import com.margin.dto.ResponseDTO;
import com.margin.dto.group.GroupCreationDTO;
import com.margin.dto.group.GroupDTO;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@PreAuthorize("hasRole('ROLE_ADMIN')")
@Path("/groups")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface GroupEndpoint {

    @GET
    @Path("/{groupId}")
    ResponseDTO<GroupDTO> get(@PathParam("groupId") Long groupId);

    @GET
    @Path("")
    ListResponseDTO<GroupDTO> getAll();

    @POST
    @Path("")
    ResponseDTO<GroupDTO> create(GroupCreationDTO groupCreationDTO);

    @PUT
    @Path("/{groupId}/add/{userId}")
    ResponseDTO<GroupDTO> addUser(@PathParam("groupId") Long groupId, @PathParam("userId") Long userId);

    @PUT
    @Path("/{groupId}/delete/{userId}")
    ResponseDTO<GroupDTO> deleteUser(@PathParam("groupId") Long groupId, @PathParam("userId") Long userId);

    @DELETE
    @Path("/{groupId}")
    ResponseDTO<Boolean> delete(@PathParam("groupId") Long groupId);

}
