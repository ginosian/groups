package com.margin.rest.group.impl;

import com.margin.BeanMapper;
import com.margin.dto.ListResponseDTO;
import com.margin.dto.ResponseDTO;
import com.margin.dto.group.GroupCreationDTO;
import com.margin.dto.group.GroupDTO;
import com.margin.rest.group.GroupEndpoint;
import com.margin.service.group.GroupService;
import com.margin.service.group.model.GroupCreationRequest;
import com.margin.service.group.model.GroupResponse;
import com.margin.service.group.model.GroupUserUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.constraints.NotNull;
import java.util.List;

public class GroupEndpointImpl implements GroupEndpoint {

    @Autowired
    private GroupService groupService;
    
    @Autowired
    private BeanMapper beanMapper;

    @Override
    public ResponseDTO<GroupDTO> get(@NotNull final Long groupId) {
        final GroupResponse groupResponse = groupService.get(groupId);
        final GroupDTO groupDTO = beanMapper.map(groupResponse, GroupDTO.class);
        return new ResponseDTO<>(null, groupDTO);
    }

    @Override
    public ListResponseDTO<GroupDTO> getAll() {
        final List<GroupResponse> groupResponses = groupService.get();
        final List<GroupDTO> groupDTOS = beanMapper.mapAsList(groupResponses, GroupDTO.class);
        return new ListResponseDTO<>(null, groupDTOS.size(), 0, 0, groupDTOS);
    }

    @Override
    public ResponseDTO<GroupDTO> create(@NotNull final GroupCreationDTO groupCreationDTO) {
        final GroupCreationRequest groupCreationRequest = beanMapper.map(groupCreationDTO, GroupCreationRequest.class);
        final GroupResponse groupResponse = groupService.create(groupCreationRequest);
        final GroupDTO groupDTO = beanMapper.map(groupResponse, GroupDTO.class);
        return new ResponseDTO<>(null, groupDTO);
    }

    @Override
    public ResponseDTO<GroupDTO> addUser(@NotNull final Long groupId, @NotNull final Long userId) {
        final GroupUserUpdateRequest request = new GroupUserUpdateRequest(groupId, userId);
        final GroupResponse groupResponse = groupService.addUser(request);
        final GroupDTO groupDTO = beanMapper.map(groupResponse, GroupDTO.class);
        return new ResponseDTO<>(null, groupDTO);
    }

    @Override
    public ResponseDTO<GroupDTO> deleteUser(@NotNull final Long groupId, @NotNull final Long userId) {
        final GroupUserUpdateRequest request = new GroupUserUpdateRequest(groupId, userId);
        final GroupResponse groupResponse = groupService.deleteUser(request);
        final GroupDTO groupDTO = beanMapper.map(groupResponse, GroupDTO.class);
        return new ResponseDTO<>(null, groupDTO);
    }

    @Override
    public ResponseDTO<Boolean> delete(@NotNull final Long groupId) {
        groupService.delete(groupId);
        return new ResponseDTO<>(null, true);
    }
}
