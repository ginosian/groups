package com.margin.rest.info.impl;

import com.margin.dto.ResponseDTO;
import com.margin.dto.info.InfoDTO;
import com.margin.rest.info.InfoEndpoint;
import com.margin.service.info.InfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InfoEndpointImpl implements InfoEndpoint {

    @Autowired
    private InfoService infoService;

    @Override
    public ResponseDTO<InfoDTO> info() {
        return new ResponseDTO<>(null, infoService.get());
    }
}
