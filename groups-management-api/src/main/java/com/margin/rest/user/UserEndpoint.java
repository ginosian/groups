package com.margin.rest.user;


import com.margin.dto.ListResponseDTO;
import com.margin.dto.ResponseDTO;
import com.margin.dto.user.UserCreationDTO;
import com.margin.dto.user.UserDTO;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@PreAuthorize("hasRole('ROLE_ADMIN')")
@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface UserEndpoint {

    @GET
    @Path("/{userId}")
    ResponseDTO<UserDTO> get(@PathParam("userId") Long userId);

    @GET
    @Path("")
    ListResponseDTO<UserDTO> get();

    @POST
    @Path("")
    ResponseDTO<UserDTO> create(UserCreationDTO userCreationDTO);

    @PUT
    @Path("")
    ResponseDTO<UserDTO> update(UserDTO userDTO);

    @PATCH
    @Path("")
    ResponseDTO<UserDTO> partialUpdate(UserDTO userDTO);

    @DELETE
    @Path("/{userId}")
    ResponseDTO<Boolean> delete(@PathParam("userId") Long userId);
}
