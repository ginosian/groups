package com.margin.rest.user.impl;

import com.margin.BeanMapper;
import com.margin.dto.ListResponseDTO;
import com.margin.dto.ResponseDTO;
import com.margin.dto.user.UserCreationDTO;
import com.margin.dto.user.UserDTO;
import com.margin.rest.user.UserEndpoint;
import com.margin.service.user.UserService;
import com.margin.service.user.model.UserCreationRequest;
import com.margin.service.user.model.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.constraints.NotNull;
import java.util.List;

public class UserEndpointImpl implements UserEndpoint {

    @Autowired
    private UserService userService;

    @Autowired
    private BeanMapper beanMapper;

    @Override
    public ResponseDTO<UserDTO> get(@NotNull final Long userId) {
        final UserResponse userResponse = userService.get(userId);
        final UserDTO userDTO = beanMapper.map(userResponse, UserDTO.class);
        return new ResponseDTO<>(null, userDTO);
    }

    @Override
    public ListResponseDTO<UserDTO> get() {
        final List<UserResponse> userResponses = userService.get();
        final List<UserDTO> userDTOS = beanMapper.mapAsList(userResponses, UserDTO.class);
        return new ListResponseDTO<>(null, userDTOS.size(), 0, 0, userDTOS);
    }

    @Override
    public ResponseDTO<UserDTO> create(@NotNull final UserCreationDTO userCreationDTO) {
        final UserCreationRequest userCreationRequest = beanMapper.map(userCreationDTO, UserCreationRequest.class);
        final UserResponse userResponse = userService.create(userCreationRequest);
        final UserDTO userDTO = beanMapper.map(userResponse, UserDTO.class);
        return new ResponseDTO<>(null, userDTO);
    }

    @Override
    public ResponseDTO<UserDTO> update(@NotNull final UserDTO userDTO) {
        return null;
    }

    @Override
    public ResponseDTO<UserDTO> partialUpdate(@NotNull final UserDTO userDTO) {
        return null;
    }

    @Override
    public ResponseDTO<Boolean> delete(@NotNull final Long userId) {
        userService.delete(userId);
        return new ResponseDTO<>(null, true);
    }
}
