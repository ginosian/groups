package com.margin.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private LocalDateTime created;

    @Column
    private LocalDateTime updated;

    @Column
    private Boolean deleted;

    @PrePersist
    public void prePersist(){
        created = LocalDateTime.now();
        deleted = false;
    }

    @PreUpdate
    public void preUpdate(){
        updated = LocalDateTime.now();
    }

}
