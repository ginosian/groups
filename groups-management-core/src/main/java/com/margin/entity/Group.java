package com.margin.entity;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Table(name = "group")
@Entity(name = "Group")
@Getter
@Setter
public class Group extends AbstractEntity {

    @Column
    private String title;

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
    private Set<ApiUser> users = new HashSet<>();

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;

        if (!(o instanceof Group)) return false;

        final Group that = (Group) o;

        return new EqualsBuilder()
                .append(getId(), that.getId())
                .append(title, that.title)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .append(title)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", getId())
                .append("title", title)
                .toString();
    }
}
