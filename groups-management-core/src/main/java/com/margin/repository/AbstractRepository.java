package com.margin.repository;


import com.margin.entity.AbstractEntity;

public interface AbstractRepository extends BaseRepository<AbstractEntity> {
}