package com.margin.repository;


import com.margin.entity.ApiUserDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApiUserDetailRepository extends JpaRepository<ApiUserDetail, Long> {

    ApiUserDetail findByUsername(String username);
}
