package com.margin.service.group;

import com.margin.service.group.model.GroupCreationRequest;
import com.margin.service.group.model.GroupResponse;
import com.margin.service.group.model.GroupUserUpdateRequest;

import java.util.List;

public interface GroupService {

    GroupResponse get(Long id);
    List<GroupResponse> get();
    GroupResponse create(GroupCreationRequest request);
    GroupResponse addUser(GroupUserUpdateRequest request);
    GroupResponse deleteUser(GroupUserUpdateRequest request);
    void delete(Long id);
}
