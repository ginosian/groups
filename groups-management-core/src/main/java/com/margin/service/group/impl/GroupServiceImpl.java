package com.margin.service.group.impl;

import com.margin.BeanMapper;
import com.margin.entity.ApiUser;
import com.margin.entity.Group;
import com.margin.repository.GroupRepository;
import com.margin.repository.UserRepository;
import com.margin.service.group.GroupService;
import com.margin.service.group.model.GroupCreationRequest;
import com.margin.service.group.model.GroupResponse;
import com.margin.service.group.model.GroupUserUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.util.Assert.hasText;
import static org.springframework.util.Assert.notNull;

@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BeanMapper beanMapper;

    @Override
    public GroupResponse get(final Long id) {
        notNull(id, "Id can not be null");
        final Group group = groupRepository.getOne(id);
        return beanMapper.map(group, GroupResponse.class);
    }

    @Override
    public List<GroupResponse> get() {
        final List<Group> groups = groupRepository.findAll();
        return beanMapper.mapAsList(groups, GroupResponse.class);
    }

    @Override
    public GroupResponse create(final GroupCreationRequest request) {
        notNull(request, "Request can not be null.");
        final String title = request.getTitle();
        hasText(title, "Title can not be null or empty");
        Group group = new Group();
        group.setTitle(title);
        group = groupRepository.save(group);
        return beanMapper.map(group, GroupResponse.class);
    }

    @Override
    public GroupResponse addUser(final GroupUserUpdateRequest request) {
        notNull(request, "Request can not be null");
        final Long groupId = request.getGroupId();
        notNull(request, "GroupId can not be null");
        final Group existingGroup = groupRepository.getOne(groupId);

        final Long userId = request.getUserId();
        notNull(request, "UserId can not be null");
        final ApiUser apiUser = userRepository.getOne(userId);

        existingGroup.getUsers().add(apiUser);
        final Group group = groupRepository.save(existingGroup);
        return beanMapper.map(group, GroupResponse.class);
    }

    @Override
    public GroupResponse deleteUser(final GroupUserUpdateRequest request) {
        notNull(request, "Request can not be null");
        final Long groupId = request.getGroupId();
        notNull(request, "GroupId can not be null");
        final Group existingGroup = groupRepository.getOne(groupId);

        final Long userId = request.getUserId();
        notNull(request, "UserId can not be null");
        final ApiUser apiUser = userRepository.getOne(userId);

        existingGroup.getUsers().remove(apiUser);
        final Group group = groupRepository.save(existingGroup);
        return beanMapper.map(group, GroupResponse.class);
    }

    @Override
    public void delete(final Long id) {
        notNull(id, "Id can not be null");
        groupRepository.deleteById(id);
    }
}
