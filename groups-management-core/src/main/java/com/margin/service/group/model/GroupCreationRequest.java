package com.margin.service.group.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GroupCreationRequest {
    private String title;
}
