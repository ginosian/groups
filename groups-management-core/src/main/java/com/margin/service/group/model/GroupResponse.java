package com.margin.service.group.model;

import com.margin.service.user.model.UserResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class GroupResponse {
    private String title;
    private Set<UserResponse> users;
}
