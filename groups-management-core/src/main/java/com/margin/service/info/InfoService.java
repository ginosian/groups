package com.margin.service.info;


import com.margin.dto.info.InfoDTO;

public interface InfoService {

    InfoDTO get();
}
