package com.margin.service.info.impl;

import com.margin.dto.info.InfoDTO;
import com.margin.service.info.InfoService;
import org.springframework.stereotype.Service;

@Service
public class InfoServiceImpl implements InfoService {

    @Override
    public InfoDTO get() {
        return new InfoDTO("This groups management application and it works just fine.");
    }
}
