package com.margin.service.token.model;

import com.margin.entity.ApiUserDetail;
import com.margin.enums.TokenType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiAuthAccessTokenCreationRequest {
    private ApiUserDetail userDetail;
    private TokenType tokenType;
    private boolean isActive;
    private Date expires;
}
