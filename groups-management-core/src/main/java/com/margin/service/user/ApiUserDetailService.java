package com.margin.service.user;


import com.margin.entity.ApiUser;
import com.margin.entity.ApiUserDetail;

public interface ApiUserDetailService {

    ApiUserDetail loadUserByUsername(String username);

    boolean isEmailUsed(String email);

    boolean isCorrectPassword(String userId, String password);

    boolean isUserActive(ApiUser user);
}
