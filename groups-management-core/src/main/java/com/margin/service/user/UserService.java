package com.margin.service.user;

import com.margin.service.user.model.UserCreationRequest;
import com.margin.service.user.model.UserResponse;

import java.util.List;

public interface UserService {

    UserResponse get(Long id);
    List<UserResponse> get();
    UserResponse create(UserCreationRequest request);
    void delete(Long id);
}
