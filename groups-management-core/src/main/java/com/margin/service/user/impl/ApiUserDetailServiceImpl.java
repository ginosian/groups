package com.margin.service.user.impl;

import com.margin.entity.ApiUser;
import com.margin.entity.ApiUserDetail;
import com.margin.repository.ApiUserDetailRepository;
import com.margin.service.user.ApiUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service(value = "api_user_detail_service")
public class ApiUserDetailServiceImpl implements ApiUserDetailService, UserDetailsService {

    @Autowired
    private ApiUserDetailRepository apiUserDetailRepository;

    @Override
    public ApiUserDetail loadUserByUsername(String username) {
        return apiUserDetailRepository.findByUsername(username);
    }

    @Override
    public boolean isEmailUsed(String email) {
        return false;
    }

    @Override
    public boolean isCorrectPassword(String userId, String password) {
        return false;
    }

    @Override
    public boolean isUserActive(ApiUser user) {
        return false;
    }
}
