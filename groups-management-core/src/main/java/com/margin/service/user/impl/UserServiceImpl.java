package com.margin.service.user.impl;

import com.margin.BeanMapper;
import com.margin.entity.ApiUser;
import com.margin.repository.UserRepository;
import com.margin.service.user.UserService;
import com.margin.service.user.model.UserCreationRequest;
import com.margin.service.user.model.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.util.Assert.hasText;
import static org.springframework.util.Assert.notNull;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BeanMapper beanMapper;

    @Override
    public UserResponse get(final Long id) {
        notNull(id, "Id can not be null");
        final ApiUser user = userRepository.getOne(id);
        return beanMapper.map(user, UserResponse.class);
    }

    @Override
    public List<UserResponse> get() {
        final List<ApiUser> users = userRepository.findAll();
        return beanMapper.mapAsList(users, UserResponse.class);
    }

    @Override
    public UserResponse create(UserCreationRequest request) {
        notNull(request, "Request can not be null.");
        final String name = request.getName();
        hasText(name, "Name can not be null or empty");
        ApiUser user = new ApiUser();
        user.setName(name);
        user = userRepository.save(user);
        return beanMapper.map(user, UserResponse.class);
    }

    @Override
    public void delete(final Long id) {
        notNull(id, "Id can not be null");
        userRepository.deleteById(id);
    }
}
