package com.margin;


import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@ImportResource("classpath:margin-grooups-web-context.xml")
@PropertySources({
        @PropertySource("classpath:application-facade.properties")
})
public class GroupsManagementApplication {

    protected GroupsManagementApplication() {
        super();
    }

    public static void main(String[] args) {
        new SpringApplicationBuilder(GroupsManagementApplication.class)
                .logStartupInfo(true)
                .bannerMode(Banner.Mode.LOG)
                .run(args);
    }
}
